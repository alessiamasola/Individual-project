﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace VRStandardAssets.Utils
{
    // This script is used to show messages when the user
    // seems to be using the wrong type of input.
    public class AlphaC2 : MonoBehaviour
    {
        // public event Action OnSelectionComplete;
        // [SerializeField] private string m_text = "This is a cube";                      // The message to display when the user uses a double tap when they shouldn't.
        [SerializeField] private Text m_WarningText;                                    // Reference to the Text component that will hold the messages.
        [SerializeField] private Image m_BackgroundImage;                               // Reference to the image that makes the background for the warning.
        [SerializeField] private Transform m_TextTransform;                             // Reference to the transform of the Text component, used to move the warning to the location of the click.
        [SerializeField] private Transform m_Camera;                                    // Reference to the camera's transform so the text knows which way to face.
        [SerializeField] private Reticle m_Reticle;                                     // Reference to the reticle in order to set the position of the warning.
        // [SerializeField] private SelectionRadial m_SelectionRadial;                     // This controls when the selection is complete.
        [SerializeField] private VRInteractiveItem m_InteractiveItem;                   // The interactive item for where the user should click to load the level.
        [SerializeField] private bool m_HideOnStart = true;                                     // Whether or not the bar should be visible at the start.
        private Coroutine m_AlphaChannelRoutine;                                               // Used to start and stop the filling coroutine based on input.
        
        private float m_ScaleMultiplier;                                                // The warning needs to have an appropriate size based on the Reticle's scale.
        public bool m_GazeOver;                                                         // Whether the user is looking at the VRInteractiveItem currently.
        public bool m_IsWindowEnabled;
        public bool m_IsAlphaChannelCompleted;
        public Vector3 retPos;
        public Color alphaValue_Image;
        public Color alphaValue_Text;
        // public float timer = 0f;
        public float m_SelectionDuration = 2f;
        public float SelectionDuration { get { return m_SelectionDuration; } }
        public int opened = 0;
        public int gazeOver = 0;
        public Vector3 localscale;  
        public Vector3 initScale;
        float timer = 0f;

        public void Start()
        {
            Debug.Log("**********************ALPHA CHANNEL***************************");
            localscale = m_BackgroundImage.transform.localScale;
            initScale = localscale;
            alphaValue_Image = m_BackgroundImage.color;
            alphaValue_Text = m_WarningText.color;
            alphaValue_Image.a = 0.0f;
            alphaValue_Text.a = 0.0f;
            m_BackgroundImage.color = alphaValue_Image;
            m_WarningText.color = alphaValue_Text;
            // Debug.Log("started");
            // Debug.Log("window position 1: " + m_BackgroundImage.transform.position.ToString());

            // m_ScaleMultiplier = m_TextTransform.localScale.x / m_Reticle.ReticleTransform.localScale.x;

            if(m_HideOnStart)
                Hide();
            // m_BackgroundImage.transform.localScale = m_BackgroundImage.transform.localScale*0.4f;
            // m_WarningText.transform.localScale = m_WarningText.transform.localScale*0.4f;            
            // m_ScaleMultiplier = m_WarningText.transform;  
        }

        void Update()
        {
            timer += Time.deltaTime;
            if(timer <= 2f && m_GazeOver)
            {
                alphaValue_Image.a = timer / m_SelectionDuration;
                alphaValue_Text.a = timer / m_SelectionDuration;
                m_BackgroundImage.color = alphaValue_Image;
                m_WarningText.color = alphaValue_Text;
                var localscale = m_WarningText.transform.localScale;
                localscale.x = timer / m_SelectionDuration;
                localscale.y = timer / m_SelectionDuration;  
                m_BackgroundImage.transform.localScale = localscale;

                // m_BackgroundImage.transform.localScale.x = timer / m_SelectionDuration;
                // m_WarningText.transform.localScale.x = timer / m_SelectionDuration;
            }
            if(!m_GazeOver)
            {
                alphaValue_Image = m_BackgroundImage.color;
                alphaValue_Text = m_WarningText.color;
                alphaValue_Image.a = 0.0f;
                alphaValue_Text.a = 0.0f;
                m_BackgroundImage.color = alphaValue_Image;
                m_WarningText.color = alphaValue_Text;
                m_BackgroundImage.transform.localScale = initScale;
            }
        }
        

        private void OnEnable()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
            // m_VRInput.OnUp += HandleUp;
        }


        private void OnDisable()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
        }

        // private IEnumerator startAlphaChannel()
        // {
        //     // Debug.Log("alpha channel started");

        //     m_IsAlphaChannelCompleted = false;
        //     m_BackgroundImage.transform.localScale = initScale;

        //     float timer = 0f;

        //     alphaValue_Image.a = 0.0f;
        //     alphaValue_Text.a = 0.0f;
        //     m_BackgroundImage.color = alphaValue_Image;
        //     m_WarningText.color = alphaValue_Text;



        //     // m_BackgroundImage.transform.position = m_Reticle.ReticleTransform.position;
        //     // m_BackgroundImage.transform.rotation = Quaternion.LookRotation (m_Camera.forward);
        //     m_TextTransform.position = m_Reticle.ReticleTransform.position;
        //     m_TextTransform.rotation = Quaternion.LookRotation (m_Camera.forward);
        //     // Debug.Log("window position 2: " + m_BackgroundImage.transform.position.ToString());

        //     // m_TextTransform.position = m_Reticle.ReticleTransform.position;
        //     // m_TextTransform.rotation = Quaternion.LookRotation (m_Camera.forward);
        //     // m_TextTransform.localScale = m_Reticle.ReticleTransform.localScale * m_ScaleMultiplier;

        //     while (timer < m_SelectionDuration)
        //     {
        //         // The image's fill amount requires a value from 0 to 1 so we normalise the time.
        //         // m_Selection.fillAmount = timer / m_SelectionDuration;
        //         if (m_GazeOver)
        //         {
        //             if(alphaValue_Image.a <= 0.85f)
        //             {
        //                 Debug.Log("still too transparent");
        //                 alphaValue_Image.a = timer / m_SelectionDuration;
        //                 alphaValue_Text.a = timer / m_SelectionDuration;
        //                 m_BackgroundImage.color = alphaValue_Image;
        //                 m_WarningText.color = alphaValue_Text;
        //             }
        //             localscale = m_BackgroundImage.transform.localScale;
        //             localscale.x = timer / m_SelectionDuration;
        //             localscale.y = timer / m_SelectionDuration;  
        //             m_BackgroundImage.transform.localScale = localscale;
        //             // Debug.Log("changing scale and color: " + localscale.ToString() + " - " + m_BackgroundImage.color.ToString());

        //             // Increase the timer by the time between frames and wait for the next frame.
        //             timer += Time.deltaTime;
        //         } else {
        //             break;
        //         }
        //         yield return null;
        //     }

        //     alphaValue_Image.a = 0.85f;
        //     alphaValue_Text.a = 0.85f;
        //     m_BackgroundImage.color = alphaValue_Image;
        //     m_WarningText.color = alphaValue_Text;
        //     localscale = m_BackgroundImage.transform.localScale;
        //     localscale.x = 1f;
        //     localscale.y = 1f;
        //     m_BackgroundImage.transform.localScale = localscale;

        //     m_IsWindowEnabled = false;
        //     m_IsAlphaChannelCompleted = true;
        //     opened += 1;
        //     Debug.Log("opened: " + opened.ToString());
        // }

        // public IEnumerator WaitForAlphaChannelToComplete()
        // {
        //     // Debug.Log("waiting for alpha channel to complete");

        //     m_IsAlphaChannelCompleted = false;

        //     Show();

        //     while(!m_IsAlphaChannelCompleted)
        //     {
        //         yield return null;
        //     }

        //     Hide();
        // }

        public void Show()
        {
            Debug.Log("showing");
            timer = 0f;
            m_TextTransform.position = m_Reticle.ReticleTransform.position;
            m_TextTransform.rotation = Quaternion.LookRotation (m_Camera.forward);
            m_BackgroundImage.enabled = true;
            m_WarningText.enabled = true;
            m_IsWindowEnabled = true;
            Update();
            
            

            // m_BackgroundImage.transform.position = m_Reticle.ReticleTransform.position; // ****new
            // m_BackgroundImage.transform.rotation = Quaternion.LookRotation (m_Camera.forward); // *****new
        }

        public void Hide()
        {
            Debug.Log("hiding");

            // m_BackgroundImage.enabled = false;
            // m_WarningText.enabled = false;
            m_IsWindowEnabled = false;
            alphaValue_Image.a = 0.0f;
            alphaValue_Text.a = 0.0f;
            m_BackgroundImage.color = alphaValue_Image;
            m_WarningText.color = alphaValue_Text;
            m_BackgroundImage.transform.localScale = initScale;

            // var localscale = m_BackgroundImage.transform.localScale;
            // localscale.x = 0;
            // localscale.y = 0;  
            // m_BackgroundImage.transform.localScale = localscale;
        }

        public void HandleOver() 
        {
            // Debug.Log("handle over");
            gazeOver += 1;
            Debug.Log("Gaze over: " + gazeOver.ToString());


            Show();

            m_GazeOver = true;

            // If the radial is active start filling it.
            // Debug.Log(m_IsWindowEnabled.ToString());
            // if (m_IsWindowEnabled)
            // {
            //     m_AlphaChannelRoutine = StartCoroutine(startAlphaChannel());
            // }
        }

        public void HandleOut() //HandleUp
        {
            Debug.Log("handle out");
            Hide();

            // When the user looks away from the rendering of the scene, hide the radial.

            m_GazeOver = false;

            // If the radial is active stop filling it and reset it's amount.
            // if (m_IsWindowEnabled)
            // {

            //     if(m_AlphaChannelRoutine != null)
            //         StopCoroutine(m_AlphaChannelRoutine);

            //     alphaValue_Image.a = 0.0f;
            //     alphaValue_Text.a = 0.0f;
            //     m_BackgroundImage.color = alphaValue_Image;
            //     m_WarningText.color = alphaValue_Text;
            //     m_BackgroundImage.transform.localScale = initScale;


            // }
            
        }
    }
}


























// void Awake()
        // {
        //     m_BackgroundImage.enabled = true;
        //     m_WarningText.enabled = true;
        //     alphaValue_Image = m_BackgroundImage.color;
        //     alphaValue_Text = m_WarningText.color;
        //     alphaValue_Image.a = 0.0f;
        //     alphaValue_Text.a = 0.0f;
        //     m_BackgroundImage.color = alphaValue_Image;
        //     m_WarningText.color = alphaValue_Text;
        //     // m_BackgroundImage.transform.localScale = m_BackgroundImage.transform.localScale*0.4f;
        //     // m_WarningText.transform.localScale = m_WarningText.transform.localScale*0.4f;          
        //     // m_ScaleMultiplier = m_WarningText.transform;  
        // }

// void Update()
        // {
        //     timer += Time.deltaTime;
        //     if(timer <= 2f)
        //     {
        //         alphaValue_Image.a = timer / m_SelectionDuration;
        //         alphaValue_Text.a = timer / m_SelectionDuration;
        //         m_BackgroundImage.color = alphaValue_Image;
        //         m_WarningText.color = alphaValue_Text;
        //         var localscale = m_WarningText.transform.localScale;
        //         localscale.x = timer / m_SelectionDuration;
        //         localscale.y = timer / m_SelectionDuration;  
        //         m_BackgroundImage.transform.localScale = localscale;

        //         // m_BackgroundImage.transform.localScale.x = timer / m_SelectionDuration;
        //         // m_WarningText.transform.localScale.x = timer / m_SelectionDuration;
        //     }
        // }