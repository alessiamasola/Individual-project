﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    [SerializeField] private TextMesh countdown;
    private float timer = 60f;
    private float startTime;
    private float elapsed;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        elapsed = Time.time - startTime;
        if (elapsed <= timer)
        {
            countdown.text = ((int)(timer - elapsed)).ToString();
        }
        
    }
}
