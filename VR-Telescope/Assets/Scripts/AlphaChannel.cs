﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace VRStandardAssets.Utils
{
    public class AlphaChannel : MonoBehaviour
    {
        [SerializeField] private Text m_WarningText;                                    // Reference to the Text component that will hold the messages.
        [SerializeField] private Image m_BackgroundImage;                               // Reference to the image that makes the background for the warning.
        [SerializeField] private Transform m_TextTransform;                             // Reference to the transform of the Text component, used to move the warning to the location of the click.
        [SerializeField] private Transform m_Camera;                                    // Reference to the camera's transform so the text knows which way to face.
        // [SerializeField] private Reticle m_Reticle;                                     // Reference to the reticle in order to set the position of the warning.
        [SerializeField] private VRInteractiveItem m_InteractiveItem;                   // The interactive item for where the user should click to load the level.
        [SerializeField] private bool m_HideOnStart = true;                                     // Whether or not the bar should be visible at the start.
        private Coroutine m_AlphaChannelRoutine;                                               // Used to start and stop the filling coroutine based on input.
        
        private float m_ScaleMultiplier;                                                // The warning needs to have an appropriate size based on the Reticle's scale.
        public bool m_GazeOver;                                                         // Whether the user is looking at the VRInteractiveItem currently.
        public bool m_IsWindowEnabled;
        public bool m_IsAlphaChannelCompleted;
        public Vector3 retPos;
        public Color alphaValue_Image;
        public Color alphaValue_Text;
        public float m_SelectionDuration = 2f;
        public float SelectionDuration { get { return m_SelectionDuration; } }
        public int opened = 0;
        public int gazeOver = 0;
        public Vector3 localscale;  
        public Vector3 initScale;
        public Vector3 localPos;
        public Vector3 initPos;

        public void Start()
        {
            Debug.Log("**********************ALPHA CHANNEL***************************");
            Debug.Log("Window size, color and position: " + m_BackgroundImage.transform.localScale.ToString() + " - " + m_BackgroundImage.color.ToString() + " - " + m_BackgroundImage.transform.position.ToString());            
            localscale = m_BackgroundImage.transform.localScale;
            localPos = m_TextTransform.position;
            initPos = localPos;
            initScale = localscale;
            alphaValue_Image = m_BackgroundImage.color;
            alphaValue_Text = m_WarningText.color;
            alphaValue_Image.a = 0.0f;
            alphaValue_Text.a = 0.0f;
            m_BackgroundImage.color = alphaValue_Image;
            m_WarningText.color = alphaValue_Text;

            if(m_HideOnStart)
                Hide();
        }

        

        private void OnEnable()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
        }


        private void OnDisable()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
        }

        private IEnumerator startAlphaChannel()
        {

            m_IsAlphaChannelCompleted = false;
            m_BackgroundImage.transform.localScale = initScale;

            float timer = 0f;

            alphaValue_Image.a = 0.0f;
            alphaValue_Text.a = 0.0f;
            m_BackgroundImage.color = alphaValue_Image;
            m_WarningText.color = alphaValue_Text;

            while (timer < m_SelectionDuration)
            {
                if (m_GazeOver)
                {
                    if(alphaValue_Image.a <= 0.784f)
                    {                        
                        alphaValue_Image.a = timer / m_SelectionDuration; 
                        alphaValue_Text.a = timer / m_SelectionDuration;
                        m_BackgroundImage.color = alphaValue_Image;
                        m_WarningText.color = alphaValue_Text;
                        if(alphaValue_Image.a >=0.78){
                            opened += 1;
                            Debug.Log("opened: " + opened.ToString());
                        }
                    }
                    localscale = m_BackgroundImage.transform.localScale;
                    localscale.x += 1.6f;
                    localscale.y += 1.6f;
                    m_BackgroundImage.transform.localScale = localscale;

                    // Increase the timer by the time between frames and wait for the next frame.
                    timer += Time.deltaTime;
                } else {
                    break;
                }
                yield return null;
            }

            alphaValue_Image.a = 0.85f;
            alphaValue_Text.a = 0.85f;
            m_BackgroundImage.color = alphaValue_Image;
            m_WarningText.color = alphaValue_Text;
            localscale = m_BackgroundImage.transform.localScale;
            localscale.x = 75f;
            localscale.y = 75f;
            m_BackgroundImage.transform.localScale = localscale;

            m_IsWindowEnabled = true;
            m_IsAlphaChannelCompleted = true;
            
        }

        public IEnumerator WaitForAlphaChannelToComplete()
        {
            m_IsAlphaChannelCompleted = false;

            Show();

            while(!m_IsAlphaChannelCompleted)
            {
                yield return null;
            }

            Hide();
        }

        // Show the panel
        public void Show()
        {
            m_BackgroundImage.enabled = true;
            m_WarningText.enabled = true;
            m_IsWindowEnabled = true;
            var IApos = m_InteractiveItem.transform.position;
            IApos.z -= 5;
            IApos.y += 4;
            m_TextTransform.position = IApos;
            m_TextTransform.rotation = Quaternion.LookRotation (m_Camera.forward);
        }

        // Hide the panel
        public void Hide()
        {
            m_TextTransform.position = initPos;
            m_BackgroundImage.enabled = false;
            m_WarningText.enabled = false;
            m_IsWindowEnabled = false;
            alphaValue_Image.a = 0.0f;
            alphaValue_Text.a = 0.0f;
            m_BackgroundImage.color = alphaValue_Image;
            m_WarningText.color = alphaValue_Text;
            m_BackgroundImage.transform.localScale = initScale;
        }

        public void HandleOver() 
        {
            gazeOver += 1;
            Debug.Log("Gaze over: " + gazeOver.ToString());


            Show();

            m_GazeOver = true;

            // If the radial is active start filling it.
            Debug.Log(m_IsWindowEnabled.ToString());
            if (m_IsWindowEnabled)
            {
                m_AlphaChannelRoutine = StartCoroutine(startAlphaChannel());
            }
        }

        public void HandleOut() 
        {
            Hide();

            // When the user looks away from the rendering of the scene, hide the radial.

            m_GazeOver = false;

            // If the radial is active stop filling it and reset it's amount.
            if (m_IsWindowEnabled)
            {

                if(m_AlphaChannelRoutine != null)
                    StopCoroutine(m_AlphaChannelRoutine);

                alphaValue_Image.a = 0.0f;
                alphaValue_Text.a = 0.0f;
                m_BackgroundImage.color = alphaValue_Image;
                m_WarningText.color = alphaValue_Text;
                m_BackgroundImage.transform.localScale = initScale;
            }
            
        }
    }
}

