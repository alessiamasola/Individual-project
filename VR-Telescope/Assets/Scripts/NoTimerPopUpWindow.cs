﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace VRStandardAssets.Utils
{
    public class NoTimerPopUpWindow : MonoBehaviour
    {
        // [SerializeField] private string m_text = "This is a cube";                      // The message to display when the user uses a double tap when they shouldn't.
        [SerializeField] private Text m_WarningText;                                    // Reference to the Text component that will hold the messages.
        [SerializeField] private Image m_BackgroundImage;                               // Reference to the image that makes the background for the warning.
        [SerializeField] private Transform m_TextTransform;                             // Reference to the transform of the Text component, used to move the warning to the location of the click.
        [SerializeField] private Transform m_Camera;                                    // Reference to the camera's transform so the text knows which way to face.
        [SerializeField] private Reticle m_Reticle;                                     // Reference to the reticle in order to set the position of the warning.
        [SerializeField] private SelectionRadial m_SelectionRadial;                     // This controls when the selection is complete.
        [SerializeField] private VRInteractiveItem m_InteractiveItem;                   // The interactive item for where the user should click to load the level.

        private float m_ScaleMultiplier;                                                // The warning needs to have an appropriate size based on the Reticle's scale.
        public bool m_GazeOver;                                                         // Whether the user is looking at the VRInteractiveItem currently.
        public bool m_IsWindowActive;                                                   // Whether the user is looking at the VRInteractiveItem currently.
        public Vector3 retPos;
        public int opened = 0;

        private void Awake()
        {
            Debug.Log("**********************NO TIMER***************************");
            // Approximate the difference in scale of the reticle and text to be the same across all axes.
            m_ScaleMultiplier = m_TextTransform.localScale.x / m_Reticle.ReticleTransform.localScale.x;

            // Display nothing on the start of the scene.
            m_BackgroundImage.enabled = false;
            m_WarningText.enabled = false;
        }


        private void OnEnable ()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
        }


        private void OnDisable ()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
        }


        private void HandleOver ()
        {
            m_GazeOver = true;
            Show();
            opened += 1;
            Debug.Log("opened: " + opened.ToString());
        }


        private void HandleOut ()
        {
            if(m_IsWindowActive)
                Hide();
            m_GazeOver = false;
        }

        private void Show()
        {
            m_WarningText.enabled = true;
            m_BackgroundImage.enabled = true;

            // Set the position of the warning to that of the Reticle.
            m_TextTransform.position = m_Reticle.ReticleTransform.position;

            // Set the rotation of the warning to facing the camera but oriented so it's up is along the global y axis.
            m_TextTransform.rotation = Quaternion.LookRotation (m_Camera.forward);
            
            // Set the scale of the warning based on the scale of the Reticle but taking the difference in scale into account.
            m_TextTransform.localScale = m_Reticle.ReticleTransform.localScale * m_ScaleMultiplier;

            m_IsWindowActive = true;

        }

        private void Hide()
        {
            m_BackgroundImage.enabled = false;
            m_WarningText.enabled = false;
            m_IsWindowActive = false;
        }
    }
}