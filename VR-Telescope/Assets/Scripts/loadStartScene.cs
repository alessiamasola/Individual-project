using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadStartScene : MonoBehaviour
{
    [SerializeField] public float timer = 30f;
    // [SerializeField] public string sceneToLoad;


    void Update()
    {
    // populateScenesHashTable();

        timer -= Time.deltaTime;
     
        if(timer <= 0)
        {
            SceneManager.LoadScene("StartScene");
        } 
    }
}