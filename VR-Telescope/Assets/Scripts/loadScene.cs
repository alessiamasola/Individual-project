﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadScene : MonoBehaviour
{
    public float timer = 4f;
    // [SerializeField] public string sceneToLoad;
    // public static int scenesCount = 2;
    public static List<string> scenesToLoad = new List<string>{"TimedReticle", "NoTimer", "AlphaChannel"};
    // public static loadScene Instance;

    // public Hashtable scenes = new Hashtable()
    // {
    //     {1, "NoTimer"},
    //     {2, "AlphaChannel"}, 
    //     {3, "EndScene"}
    // };
    // public List<int> loadedScenes = new List<int>();
    // private string sceneToLoad;

    // void Start()
    // {
    //     timer = 4f;
    //     // scenes.Add(1, "TimedReticle");
    //     // scenes.Add(2, "NoTimer");
    //     // scenes.Add(3, "AlphaChannel");
    //     // scenes.Add(4, "EndScene");
    // }

    public int generateScene(int scenesCount)
    {
        System.Random r = new System.Random(); 
        int randomScene = r.Next(0, scenesCount);
        return randomScene;
    }
    
    // void Awake()
    // {
    //     Debug.Log("Awakened");
    //     Debug.Log(scenesCount);
    //     // scenesCount = 3;
    //     // scenesToLoad = new List<string>{"TimedReticle", "NoTimer", "AlphaChannel"};
    // }

    // void Start()
    // {
    //     Debug.Log("Awakened");
    //     Debug.Log(scenesCount);
    //     scenesToLoad = GlobalControl.Instance.scenesToLoad;
    //     scenesCount = GlobalControl.Instance.scenesCount;
    // }

    void Update()
    {
    // populateScenesHashTable();

        timer -= Time.deltaTime;
     
        if(timer <= 0 && scenesToLoad.Count > 0) 
        {
        //     int randomLoadedScene = generateScene();
        //     while (loadedScenes.Contains(randomLoadedScene))
        //     {
        //         randomLoadedScene = generateScene();
        //     }
        //     loadedScenes.Add(randomLoadedScene); 
        //  
        //     SceneManager.LoadScene((string)(scenes[randomLoadedScene]));
        //     scenesCount--;
        // } 
        // else if (scenesCount == 1)
        // {
        //     SceneManager.LoadScene("endScene");
            // int i = Random.Range(0,scenesCount-1);
            int i = generateScene(scenesToLoad.Count);
            string loadedScene = scenesToLoad[i];
            // scenesCount--;
            scenesToLoad.RemoveAt(i);
            SceneManager.LoadScene(loadedScene);
        } 
        // else if (scenesToLoad.Count == 0)
        // {
        //     SceneManager.LoadScene("endScene");
        // }
        
    }
    // Save data to global control   
    // public void SaveData()
    // {
    //     GlobalControl.Instance.scenesToLoad = scenesToLoad;
    //     GlobalControl.Instance.scenesCount = scenesCount;
    // }
}

// Application.LoadLevel ("....");

// SceneManager.LoadScene(m_MenuSceneName, LoadSceneMode.Single);