﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    [SerializeField] public Text startText;
    [SerializeField] public Text endText;
    [SerializeField] private string start = "Please wait until the next scene is loaded.";                       // The message to display when the user uses a double tap when they shouldn't.
    [SerializeField] private string end = "The experiment is over.\nThank you!";                         // The message to display when the user uses a double tap when they shouldn't.    
    // [SerializeField] public bool Startactive;
    // [SerializeField] public bool Endactive;

    public static int reloadedNum = 0;
    string count;



    // Start is called before the first frame update
    void Start()
    {
        // count = reloadedNum.ToString();
        startText.text = start;
        startText.gameObject.SetActive(true);
        endText.gameObject.SetActive(false);
        reloadedNum++;
        textControl();
    }

    void textControl()
    {
        if(reloadedNum < 7)
        {
            // count = reloadedNum.ToString();
            startText.text = start;
            startText.gameObject.SetActive(true);
            endText.gameObject.SetActive(false);
        } else {
            // count = reloadedNum.ToString();
            endText.text = end;
            startText.gameObject.SetActive(false);
            endText.gameObject.SetActive(true);
        }
    }

    // void Awake()
    // {
    //     text = "Please wait until the next scene is loaded.";
    // }

    // Update is called once per frame
    // void Update()
    // {
    //     timer -= Time.deltaTime;
    //     if (timer > 0)
    //     {
    //         startText.gameObject.SetActive(true);
    //         endText.gameObject.SetActive(false);
    //     } else {
    //         // text = "The experiment is over.\nThank you!";
    //         startText.gameObject.SetActive(false);
    //         endText.gameObject.SetActive(true);
    //     }
        
    // }
}
